import psycopg2
from psycopg2.extras import execute_batch
from functools import wraps

con = psycopg2.connect(host="localhost",
                       database="experience",
                       user='postgres',
                       password='1234',
                       port=5432)


def insert_de_masse(df, table, conn):
    # crée une liste de colonnes
    df_colonnes = list(df)
    colonnes = ",".join(df_colonnes)

    # crée un values aves des %s pour les valeurs
    values = "VALUES({})".format(",".join(["%s" for colonnes in df_colonnes]))

    # prépare le satetement correctement
    insert = "INSERT INTO {} ({}) {}".format(table, colonnes, values)
    print(insert)
    cur = conn.cursor()

    # rempli le string avec les valeurs des la df et execute
    psycopg2.extras.execute_batch(cur, insert, df.values)
    conn.commit()
    cur.close()