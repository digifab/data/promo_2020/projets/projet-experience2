import sched   
from datetime import datetime, timedelta
import os

def sys_save():
    if os.name == "posix":
        os.system("""
            PATH="${PATH}:/Library/PostgreSQL/12/bin";
            PGPASSWORD=1234 pg_dump -U postgres -F p experience > backup.sql""")
    elif os.name == 'nt':
        os.system("""
            PATH="${PATH}:\Library/PostgreSQL\12\bin";
            PGPASSWORD=1234 pg_dump -U postgres -F p experience > backup.sql""")

debut = datetime.now()        
s = sched.scheduler()

while datetime.now() < debut + timedelta(seconds=5):
    if debut < datetime.now():
        print("debut = ", debut)
        print("debut + 5", debut+timedelta(seconds=5))
    s.enter(2, 1, sys_save)
    s.run()