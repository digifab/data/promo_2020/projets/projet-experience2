from peewee import Model, JOIN
from peewee import (AutoField, TextField, ForeignKeyField, FloatField,
                    DateField, IntegerField, DateTimeField)
from playhouse.postgres_ext import PostgresqlExtDatabase, ArrayField

import pandas as pd
import datetime
import insert
db = PostgresqlExtDatabase(host="localhost",
                           database="experience",
                           user='postgres',
                           password='1234',
                           port=5432)


class BaseModel(Model):
    class Meta():
        database = db


class Auteur(BaseModel):
    id_auteur = AutoField()
    auteur = TextField()


class Graphique(BaseModel):
    id_graph = AutoField()
    type_graph = TextField()
    date = DateTimeField()
    id_auteur = ForeignKeyField(Auteur)


class Valeur_type1(BaseModel):
    id_valeur = AutoField()
    position  = FloatField()
    x = FloatField()
    type_graph = TextField()
    nb_scan = IntegerField()
    id_graph = ForeignKeyField(Graphique)

class Valeur_type2(BaseModel):
    id_valeur = AutoField()
    position = FloatField()
    y = FloatField()
    type_graph = TextField()
    nb_scan = IntegerField()
    id_graph = ForeignKeyField(Graphique)

class Commentaire(BaseModel):
    id_commentaire = AutoField()
    texte = TextField()
    id_graph = ForeignKeyField(Graphique)

db.create_tables([Auteur, Graphique, Valeur_type1, Valeur_type2, Commentaire])

def insert_auteur(nom_auteur):
    person, created = Auteur.get_or_create(
        auteur=nom_auteur
    )

def insert_graphique(type_graphique,auteur):
    id_auteur_id = Auteur.get(Auteur.auteur == auteur)
    graph = Graphique.create(type_graph=type_graphique,date=datetime.datetime.today(),id_auteur=id_auteur_id)

def insert_commentaire(text_valeur, id_graphique):
    com = Commentaire.create(texte=text_valeur, id_graph=id_graphique)


if __name__ == '__main__':
    creation_table = False
    if creation_table:
        # pour créer la table
        db.drop_tables([Auteur, Graphique, Valeur_type1,Valeur_type2, Commentaire])

        
