@ASCII_DATA_FILE
NCurves=12
NPoints=121
Title=very well reproduced with clearly enhanced oscillations when probe rotated
SubTitle=2020-09-09 10:04:49.544843
XLabel=Axe X en fs
YLabel=
FWHM=0,000000
fitform=0
@END_HEADER

Pas de commentaire
# position	x	type_graph	nb_graph	id_grap	position	y	type_graph	nb_graph	id_graph
-3.0	0.4257879578414321	Reel	1	1	-3.0	0.5046248436369564	Imaginaire	1	1
-1.0	0.6977775871915954	Reel	1	1	-1.0	0.029913309668116894	Imaginaire	1	1
1.0	0.2503692935747237	Reel	1	1	1.0	0.24232327359085148	Imaginaire	1	1
3.0	0.604626434586156	Reel	1	1	3.0	0.058663767495247154	Imaginaire	1	1
5.0	0.7049182323983368	Reel	1	1	5.0	0.031151345516709394	Imaginaire	1	1
7.0	0.90466075688696	Reel	1	1	7.0	0.25156188040702454	Imaginaire	1	1
9.0	0.6760745574473527	Reel	1	1	9.0	0.5118896259730774	Imaginaire	1	1
11.0	0.5767849560119568	Reel	1	1	11.0	0.8310193686872163	Imaginaire	1	1
