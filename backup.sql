--
-- PostgreSQL database dump
--

-- Dumped from database version 12.4
-- Dumped by pg_dump version 12.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: auteur; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auteur (
    id_auteur integer NOT NULL,
    auteur text NOT NULL
);


ALTER TABLE public.auteur OWNER TO postgres;

--
-- Name: auteur_id_auteur_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auteur_id_auteur_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auteur_id_auteur_seq OWNER TO postgres;

--
-- Name: auteur_id_auteur_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auteur_id_auteur_seq OWNED BY public.auteur.id_auteur;


--
-- Name: commentaire; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.commentaire (
    id_commentaire integer NOT NULL,
    texte text NOT NULL,
    id_graph_id integer NOT NULL
);


ALTER TABLE public.commentaire OWNER TO postgres;

--
-- Name: commentaire_id_commentaire_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.commentaire_id_commentaire_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.commentaire_id_commentaire_seq OWNER TO postgres;

--
-- Name: commentaire_id_commentaire_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.commentaire_id_commentaire_seq OWNED BY public.commentaire.id_commentaire;


--
-- Name: graphique; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.graphique (
    id_graph integer NOT NULL,
    type_graph text NOT NULL,
    date timestamp without time zone NOT NULL,
    id_auteur_id integer NOT NULL
);


ALTER TABLE public.graphique OWNER TO postgres;

--
-- Name: graphique_id_graph_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.graphique_id_graph_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.graphique_id_graph_seq OWNER TO postgres;

--
-- Name: graphique_id_graph_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.graphique_id_graph_seq OWNED BY public.graphique.id_graph;


--
-- Name: valeur_type1; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.valeur_type1 (
    id_valeur integer NOT NULL,
    x integer NOT NULL,
    y integer NOT NULL,
    type_graph text NOT NULL,
    nb_scan integer NOT NULL,
    id_graph_id integer NOT NULL
);


ALTER TABLE public.valeur_type1 OWNER TO postgres;

--
-- Name: valeur_type1_id_valeur_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.valeur_type1_id_valeur_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.valeur_type1_id_valeur_seq OWNER TO postgres;

--
-- Name: valeur_type1_id_valeur_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.valeur_type1_id_valeur_seq OWNED BY public.valeur_type1.id_valeur;


--
-- Name: valeur_type2; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.valeur_type2 (
    id_valeur integer NOT NULL,
    x integer NOT NULL,
    y integer NOT NULL,
    type_graph text NOT NULL,
    nb_scan integer NOT NULL,
    id_graph_id integer NOT NULL
);


ALTER TABLE public.valeur_type2 OWNER TO postgres;

--
-- Name: valeur_type2_id_valeur_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.valeur_type2_id_valeur_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.valeur_type2_id_valeur_seq OWNER TO postgres;

--
-- Name: valeur_type2_id_valeur_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.valeur_type2_id_valeur_seq OWNED BY public.valeur_type2.id_valeur;


--
-- Name: auteur id_auteur; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auteur ALTER COLUMN id_auteur SET DEFAULT nextval('public.auteur_id_auteur_seq'::regclass);


--
-- Name: commentaire id_commentaire; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.commentaire ALTER COLUMN id_commentaire SET DEFAULT nextval('public.commentaire_id_commentaire_seq'::regclass);


--
-- Name: graphique id_graph; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.graphique ALTER COLUMN id_graph SET DEFAULT nextval('public.graphique_id_graph_seq'::regclass);


--
-- Name: valeur_type1 id_valeur; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.valeur_type1 ALTER COLUMN id_valeur SET DEFAULT nextval('public.valeur_type1_id_valeur_seq'::regclass);


--
-- Name: valeur_type2 id_valeur; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.valeur_type2 ALTER COLUMN id_valeur SET DEFAULT nextval('public.valeur_type2_id_valeur_seq'::regclass);


--
-- Data for Name: auteur; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auteur (id_auteur, auteur) FROM stdin;
1	leeroy
2	mannier
3	test
4	moi
5	insertion
6	unknow
\.


--
-- Data for Name: commentaire; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.commentaire (id_commentaire, texte, id_graph_id) FROM stdin;
1	Pas de commentaire	1
2	marrant	2
3	Pas de commentaire	2
4	Pas de commentaire	3
5	Pas de commentaire	4
6	Pas de commentaire	5
7	Pas de commentaire	5
8	Pas de commentaire	8
9	Pas de commentaire	10
10	Pas de commentaire	11
11	Pas de commentaire	12
12	Pas de commentaire	13
13	Pas de commentaire	13
14	Pas de commentaire	14
\.


--
-- Data for Name: graphique; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.graphique (id_graph, type_graph, date, id_auteur_id) FROM stdin;
1	Phase/Amplitude	2020-09-07 20:58:53.709727	1
2	Reel/Imaginaire	2020-09-07 21:03:23.147888	2
3	Phase/Amplitude	2020-09-07 21:03:53.169898	1
4	Reel/Imaginaire	2020-09-07 21:15:16.810576	3
5	Phase/Amplitude	2020-09-07 21:15:52.028746	3
6	Phase/Amplitude	2020-09-07 21:25:34.856444	4
7	Phase/Amplitude	2020-09-07 21:43:42.603092	5
8	Phase/Amplitude	2020-09-07 23:26:16.510091	6
9	Phase/Amplitude	2020-09-07 23:26:53.975722	6
10	Phase/Amplitude	2020-09-07 23:28:48.492606	6
11	Phase/Amplitude	2020-09-07 23:28:58.056356	6
12	Phase/Amplitude	2020-09-07 23:29:50.565432	6
13	Phase/Amplitude	2020-09-07 23:29:55.725152	6
14	Phase/Amplitude	2020-09-07 23:30:11.589742	6
\.


--
-- Data for Name: valeur_type1; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.valeur_type1 (id_valeur, x, y, type_graph, nb_scan, id_graph_id) FROM stdin;
1	5	95	Phase	1	1
2	7	57	Phase	1	1
3	9	38	Phase	1	1
4	11	79	Phase	1	1
5	13	33	Phase	1	1
6	15	96	Phase	1	1
7	17	13	Phase	1	1
8	19	67	Phase	1	1
9	21	40	Phase	1	1
10	23	88	Phase	1	1
11	25	8	Phase	1	1
12	27	84	Phase	1	1
13	5	91	Phase	2	1
14	7	66	Phase	2	1
15	9	1	Phase	2	1
16	11	46	Phase	2	1
17	13	56	Phase	2	1
18	15	22	Phase	2	1
19	17	2	Phase	2	1
20	19	89	Phase	2	1
21	21	10	Phase	2	1
22	23	88	Phase	2	1
23	25	45	Phase	2	1
24	27	58	Phase	2	1
25	5	73	Phase	3	1
26	7	10	Phase	3	1
27	9	97	Phase	3	1
28	11	85	Phase	3	1
29	13	31	Phase	3	1
30	15	24	Phase	3	1
31	17	15	Phase	3	1
32	19	1	Phase	3	1
33	21	6	Phase	3	1
34	23	45	Phase	3	1
35	25	45	Phase	3	1
36	27	59	Phase	3	1
37	5	98	Phase	4	1
38	7	77	Phase	4	1
39	9	44	Phase	4	1
40	11	20	Phase	4	1
41	13	3	Phase	4	1
42	15	27	Phase	4	1
43	17	26	Phase	4	1
44	19	31	Phase	4	1
45	21	15	Phase	4	1
46	23	90	Phase	4	1
47	25	43	Phase	4	1
48	27	95	Phase	4	1
49	5	74	Phase	5	1
50	7	20	Phase	5	1
51	9	41	Phase	5	1
52	11	18	Phase	5	1
53	13	97	Phase	5	1
54	15	74	Phase	5	1
55	17	66	Phase	5	1
56	19	7	Phase	5	1
57	21	72	Phase	5	1
58	23	98	Phase	5	1
59	25	37	Phase	5	1
60	27	90	Phase	5	1
61	5	20	Phase	6	1
62	7	2	Phase	6	1
63	9	50	Phase	6	1
64	11	54	Phase	6	1
65	13	20	Phase	6	1
66	15	38	Phase	6	1
67	17	56	Phase	6	1
68	19	59	Phase	6	1
69	21	33	Phase	6	1
70	23	18	Phase	6	1
71	25	80	Phase	6	1
72	27	96	Phase	6	1
73	5	49	Reel	1	2
74	7	79	Reel	1	2
75	9	16	Reel	1	2
76	11	22	Reel	1	2
77	13	77	Reel	1	2
78	15	64	Reel	1	2
79	17	63	Reel	1	2
80	19	79	Reel	1	2
81	21	69	Reel	1	2
82	23	97	Reel	1	2
83	25	88	Reel	1	2
84	27	27	Reel	1	2
85	5	83	Phase	1	3
86	7	26	Phase	1	3
87	9	91	Phase	1	3
88	11	36	Phase	1	3
89	13	96	Phase	1	3
90	15	49	Phase	1	3
91	17	67	Phase	1	3
92	19	30	Phase	1	3
93	21	82	Phase	1	3
94	23	13	Phase	1	3
95	25	16	Phase	1	3
96	27	95	Phase	1	3
97	5	80	Reel	1	4
98	7	51	Reel	1	4
99	9	89	Reel	1	4
100	11	52	Reel	1	4
101	13	77	Reel	1	4
102	15	57	Reel	1	4
103	17	40	Reel	1	4
104	19	60	Reel	1	4
105	21	33	Reel	1	4
106	23	4	Reel	1	4
107	25	27	Reel	1	4
108	27	69	Reel	1	4
109	5	29	Reel	2	4
110	7	67	Reel	2	4
111	9	46	Reel	2	4
112	11	89	Reel	2	4
113	13	31	Reel	2	4
114	15	31	Reel	2	4
115	17	95	Reel	2	4
116	19	68	Reel	2	4
117	21	60	Reel	2	4
118	23	86	Reel	2	4
119	25	12	Reel	2	4
120	27	35	Reel	2	4
121	5	22	Phase	1	5
122	7	59	Phase	1	5
123	9	79	Phase	1	5
124	11	70	Phase	1	5
125	13	41	Phase	1	5
126	15	21	Phase	1	5
127	17	83	Phase	1	5
128	19	83	Phase	1	5
129	21	27	Phase	1	5
130	23	13	Phase	1	5
131	25	37	Phase	1	5
132	27	72	Phase	1	5
133	5	38	Phase	2	5
134	7	49	Phase	2	5
135	9	21	Phase	2	5
136	11	45	Phase	2	5
137	13	91	Phase	2	5
138	15	30	Phase	2	5
139	17	12	Phase	2	5
140	19	33	Phase	2	5
141	21	51	Phase	2	5
142	23	67	Phase	2	5
143	25	35	Phase	2	5
144	27	89	Phase	2	5
145	5	54	Phase	3	5
146	7	69	Phase	3	5
147	9	73	Phase	3	5
148	11	63	Phase	3	5
149	13	8	Phase	3	5
150	15	97	Phase	3	5
151	17	16	Phase	3	5
152	19	63	Phase	3	5
153	21	34	Phase	3	5
154	23	51	Phase	3	5
155	25	22	Phase	3	5
156	27	28	Phase	3	5
157	5	94	Phase	4	5
158	7	44	Phase	4	5
159	9	49	Phase	4	5
160	11	62	Phase	4	5
161	13	60	Phase	4	5
162	15	9	Phase	4	5
163	17	40	Phase	4	5
164	19	60	Phase	4	5
165	21	89	Phase	4	5
166	23	86	Phase	4	5
167	25	71	Phase	4	5
168	27	32	Phase	4	5
169	5	57	Phase	1	10
170	7	65	Phase	1	10
171	9	89	Phase	1	10
172	11	67	Phase	1	10
173	13	54	Phase	1	10
174	15	79	Phase	1	10
175	17	76	Phase	1	10
176	19	0	Phase	1	10
177	21	55	Phase	1	10
178	23	71	Phase	1	10
179	25	49	Phase	1	10
180	27	21	Phase	1	10
181	29	77	Phase	1	10
182	31	59	Phase	1	10
183	5	5	Phase	2	10
184	7	78	Phase	2	10
185	9	31	Phase	2	10
186	11	11	Phase	2	10
187	13	41	Phase	2	10
188	15	86	Phase	2	10
189	17	14	Phase	2	10
190	19	19	Phase	2	10
191	21	15	Phase	2	10
192	23	76	Phase	2	10
193	25	40	Phase	2	10
194	27	22	Phase	2	10
195	29	19	Phase	2	10
196	31	62	Phase	2	10
197	5	15	Phase	1	11
198	7	65	Phase	1	11
199	9	39	Phase	1	11
200	11	97	Phase	1	11
201	13	15	Phase	1	11
202	15	48	Phase	1	11
203	17	59	Phase	1	11
204	19	84	Phase	1	11
205	21	69	Phase	1	11
206	23	52	Phase	1	11
207	25	26	Phase	1	11
208	27	23	Phase	1	11
209	29	86	Phase	1	11
210	31	9	Phase	1	11
211	5	100	Phase	2	11
212	7	86	Phase	2	11
213	9	37	Phase	2	11
214	11	13	Phase	2	11
215	13	19	Phase	2	11
216	15	0	Phase	2	11
217	17	52	Phase	2	11
218	19	23	Phase	2	11
219	21	40	Phase	2	11
220	23	83	Phase	2	11
221	25	4	Phase	2	11
222	27	20	Phase	2	11
223	29	81	Phase	2	11
224	31	47	Phase	2	11
225	5	29	Phase	1	12
226	7	75	Phase	1	12
227	9	26	Phase	1	12
228	11	73	Phase	1	12
229	13	34	Phase	1	12
230	15	2	Phase	1	12
231	17	2	Phase	1	12
232	19	65	Phase	1	12
233	21	64	Phase	1	12
234	23	100	Phase	1	12
235	25	63	Phase	1	12
236	27	29	Phase	1	12
237	29	74	Phase	1	12
238	31	69	Phase	1	12
239	5	49	Phase	2	12
240	7	75	Phase	2	12
241	9	6	Phase	2	12
242	11	75	Phase	2	12
243	13	74	Phase	2	12
244	15	16	Phase	2	12
245	17	80	Phase	2	12
246	19	9	Phase	2	12
247	21	2	Phase	2	12
248	23	88	Phase	2	12
249	25	98	Phase	2	12
250	27	27	Phase	2	12
251	29	53	Phase	2	12
252	31	94	Phase	2	12
253	5	19	Phase	1	13
254	7	37	Phase	1	13
255	9	35	Phase	1	13
256	11	37	Phase	1	13
257	13	4	Phase	1	13
258	15	72	Phase	1	13
259	17	91	Phase	1	13
260	19	46	Phase	1	13
261	21	46	Phase	1	13
262	23	41	Phase	1	13
263	25	54	Phase	1	13
264	27	8	Phase	1	13
265	29	99	Phase	1	13
266	31	31	Phase	1	13
267	5	20	Phase	2	13
268	7	80	Phase	2	13
269	9	14	Phase	2	13
270	11	29	Phase	2	13
271	13	83	Phase	2	13
272	15	87	Phase	2	13
273	17	92	Phase	2	13
274	19	74	Phase	2	13
275	21	34	Phase	2	13
276	23	89	Phase	2	13
277	25	44	Phase	2	13
278	27	70	Phase	2	13
279	29	45	Phase	2	13
280	31	47	Phase	2	13
281	5	28	Phase	1	14
282	7	12	Phase	1	14
283	9	22	Phase	1	14
284	11	65	Phase	1	14
285	13	83	Phase	1	14
286	15	9	Phase	1	14
287	17	26	Phase	1	14
288	19	7	Phase	1	14
289	21	21	Phase	1	14
290	23	60	Phase	1	14
291	25	81	Phase	1	14
292	27	88	Phase	1	14
293	29	58	Phase	1	14
294	31	15	Phase	1	14
295	5	77	Phase	2	14
296	7	28	Phase	2	14
297	9	40	Phase	2	14
298	11	10	Phase	2	14
299	13	0	Phase	2	14
300	15	57	Phase	2	14
301	17	90	Phase	2	14
302	19	93	Phase	2	14
303	21	45	Phase	2	14
304	23	3	Phase	2	14
305	25	76	Phase	2	14
306	27	66	Phase	2	14
307	29	38	Phase	2	14
308	31	81	Phase	2	14
\.


--
-- Data for Name: valeur_type2; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.valeur_type2 (id_valeur, x, y, type_graph, nb_scan, id_graph_id) FROM stdin;
1	5	95	Amplitude	1	1
2	7	57	Amplitude	1	1
3	9	38	Amplitude	1	1
4	11	79	Amplitude	1	1
5	13	33	Amplitude	1	1
6	15	96	Amplitude	1	1
7	17	13	Amplitude	1	1
8	19	67	Amplitude	1	1
9	21	40	Amplitude	1	1
10	23	88	Amplitude	1	1
11	25	8	Amplitude	1	1
12	27	84	Amplitude	1	1
13	5	91	Amplitude	2	1
14	7	66	Amplitude	2	1
15	9	1	Amplitude	2	1
16	11	46	Amplitude	2	1
17	13	56	Amplitude	2	1
18	15	22	Amplitude	2	1
19	17	2	Amplitude	2	1
20	19	89	Amplitude	2	1
21	21	10	Amplitude	2	1
22	23	88	Amplitude	2	1
23	25	45	Amplitude	2	1
24	27	58	Amplitude	2	1
25	5	73	Amplitude	3	1
26	7	10	Amplitude	3	1
27	9	97	Amplitude	3	1
28	11	85	Amplitude	3	1
29	13	31	Amplitude	3	1
30	15	24	Amplitude	3	1
31	17	15	Amplitude	3	1
32	19	1	Amplitude	3	1
33	21	6	Amplitude	3	1
34	23	45	Amplitude	3	1
35	25	45	Amplitude	3	1
36	27	59	Amplitude	3	1
37	5	98	Amplitude	4	1
38	7	77	Amplitude	4	1
39	9	44	Amplitude	4	1
40	11	20	Amplitude	4	1
41	13	3	Amplitude	4	1
42	15	27	Amplitude	4	1
43	17	26	Amplitude	4	1
44	19	31	Amplitude	4	1
45	21	15	Amplitude	4	1
46	23	90	Amplitude	4	1
47	25	43	Amplitude	4	1
48	27	95	Amplitude	4	1
49	5	74	Amplitude	5	1
50	7	20	Amplitude	5	1
51	9	41	Amplitude	5	1
52	11	18	Amplitude	5	1
53	13	97	Amplitude	5	1
54	15	74	Amplitude	5	1
55	17	66	Amplitude	5	1
56	19	7	Amplitude	5	1
57	21	72	Amplitude	5	1
58	23	98	Amplitude	5	1
59	25	37	Amplitude	5	1
60	27	90	Amplitude	5	1
61	5	20	Amplitude	6	1
62	7	2	Amplitude	6	1
63	9	50	Amplitude	6	1
64	11	54	Amplitude	6	1
65	13	20	Amplitude	6	1
66	15	38	Amplitude	6	1
67	17	56	Amplitude	6	1
68	19	59	Amplitude	6	1
69	21	33	Amplitude	6	1
70	23	18	Amplitude	6	1
71	25	80	Amplitude	6	1
72	27	96	Amplitude	6	1
73	5	49	Imaginaire	1	2
74	7	79	Imaginaire	1	2
75	9	16	Imaginaire	1	2
76	11	22	Imaginaire	1	2
77	13	77	Imaginaire	1	2
78	15	64	Imaginaire	1	2
79	17	63	Imaginaire	1	2
80	19	79	Imaginaire	1	2
81	21	69	Imaginaire	1	2
82	23	97	Imaginaire	1	2
83	25	88	Imaginaire	1	2
84	27	27	Imaginaire	1	2
85	5	83	Amplitude	1	3
86	7	26	Amplitude	1	3
87	9	91	Amplitude	1	3
88	11	36	Amplitude	1	3
89	13	96	Amplitude	1	3
90	15	49	Amplitude	1	3
91	17	67	Amplitude	1	3
92	19	30	Amplitude	1	3
93	21	82	Amplitude	1	3
94	23	13	Amplitude	1	3
95	25	16	Amplitude	1	3
96	27	95	Amplitude	1	3
97	5	80	Imaginaire	1	4
98	7	51	Imaginaire	1	4
99	9	89	Imaginaire	1	4
100	11	52	Imaginaire	1	4
101	13	77	Imaginaire	1	4
102	15	57	Imaginaire	1	4
103	17	40	Imaginaire	1	4
104	19	60	Imaginaire	1	4
105	21	33	Imaginaire	1	4
106	23	4	Imaginaire	1	4
107	25	27	Imaginaire	1	4
108	27	69	Imaginaire	1	4
109	5	29	Imaginaire	2	4
110	7	67	Imaginaire	2	4
111	9	46	Imaginaire	2	4
112	11	89	Imaginaire	2	4
113	13	31	Imaginaire	2	4
114	15	31	Imaginaire	2	4
115	17	95	Imaginaire	2	4
116	19	68	Imaginaire	2	4
117	21	60	Imaginaire	2	4
118	23	86	Imaginaire	2	4
119	25	12	Imaginaire	2	4
120	27	35	Imaginaire	2	4
121	5	22	Amplitude	1	5
122	7	59	Amplitude	1	5
123	9	79	Amplitude	1	5
124	11	70	Amplitude	1	5
125	13	41	Amplitude	1	5
126	15	21	Amplitude	1	5
127	17	83	Amplitude	1	5
128	19	83	Amplitude	1	5
129	21	27	Amplitude	1	5
130	23	13	Amplitude	1	5
131	25	37	Amplitude	1	5
132	27	72	Amplitude	1	5
133	5	38	Amplitude	2	5
134	7	49	Amplitude	2	5
135	9	21	Amplitude	2	5
136	11	45	Amplitude	2	5
137	13	91	Amplitude	2	5
138	15	30	Amplitude	2	5
139	17	12	Amplitude	2	5
140	19	33	Amplitude	2	5
141	21	51	Amplitude	2	5
142	23	67	Amplitude	2	5
143	25	35	Amplitude	2	5
144	27	89	Amplitude	2	5
145	5	54	Amplitude	3	5
146	7	69	Amplitude	3	5
147	9	73	Amplitude	3	5
148	11	63	Amplitude	3	5
149	13	8	Amplitude	3	5
150	15	97	Amplitude	3	5
151	17	16	Amplitude	3	5
152	19	63	Amplitude	3	5
153	21	34	Amplitude	3	5
154	23	51	Amplitude	3	5
155	25	22	Amplitude	3	5
156	27	28	Amplitude	3	5
157	5	94	Amplitude	4	5
158	7	44	Amplitude	4	5
159	9	49	Amplitude	4	5
160	11	62	Amplitude	4	5
161	13	60	Amplitude	4	5
162	15	9	Amplitude	4	5
163	17	40	Amplitude	4	5
164	19	60	Amplitude	4	5
165	21	89	Amplitude	4	5
166	23	86	Amplitude	4	5
167	25	71	Amplitude	4	5
168	27	32	Amplitude	4	5
169	5	57	Amplitude	1	10
170	7	65	Amplitude	1	10
171	9	89	Amplitude	1	10
172	11	67	Amplitude	1	10
173	13	54	Amplitude	1	10
174	15	79	Amplitude	1	10
175	17	76	Amplitude	1	10
176	19	0	Amplitude	1	10
177	21	55	Amplitude	1	10
178	23	71	Amplitude	1	10
179	25	49	Amplitude	1	10
180	27	21	Amplitude	1	10
181	29	77	Amplitude	1	10
182	31	59	Amplitude	1	10
183	5	5	Amplitude	2	10
184	7	78	Amplitude	2	10
185	9	31	Amplitude	2	10
186	11	11	Amplitude	2	10
187	13	41	Amplitude	2	10
188	15	86	Amplitude	2	10
189	17	14	Amplitude	2	10
190	19	19	Amplitude	2	10
191	21	15	Amplitude	2	10
192	23	76	Amplitude	2	10
193	25	40	Amplitude	2	10
194	27	22	Amplitude	2	10
195	29	19	Amplitude	2	10
196	31	62	Amplitude	2	10
197	5	15	Amplitude	1	11
198	7	65	Amplitude	1	11
199	9	39	Amplitude	1	11
200	11	97	Amplitude	1	11
201	13	15	Amplitude	1	11
202	15	48	Amplitude	1	11
203	17	59	Amplitude	1	11
204	19	84	Amplitude	1	11
205	21	69	Amplitude	1	11
206	23	52	Amplitude	1	11
207	25	26	Amplitude	1	11
208	27	23	Amplitude	1	11
209	29	86	Amplitude	1	11
210	31	9	Amplitude	1	11
211	5	100	Amplitude	2	11
212	7	86	Amplitude	2	11
213	9	37	Amplitude	2	11
214	11	13	Amplitude	2	11
215	13	19	Amplitude	2	11
216	15	0	Amplitude	2	11
217	17	52	Amplitude	2	11
218	19	23	Amplitude	2	11
219	21	40	Amplitude	2	11
220	23	83	Amplitude	2	11
221	25	4	Amplitude	2	11
222	27	20	Amplitude	2	11
223	29	81	Amplitude	2	11
224	31	47	Amplitude	2	11
225	5	29	Amplitude	1	12
226	7	75	Amplitude	1	12
227	9	26	Amplitude	1	12
228	11	73	Amplitude	1	12
229	13	34	Amplitude	1	12
230	15	2	Amplitude	1	12
231	17	2	Amplitude	1	12
232	19	65	Amplitude	1	12
233	21	64	Amplitude	1	12
234	23	100	Amplitude	1	12
235	25	63	Amplitude	1	12
236	27	29	Amplitude	1	12
237	29	74	Amplitude	1	12
238	31	69	Amplitude	1	12
239	5	49	Amplitude	2	12
240	7	75	Amplitude	2	12
241	9	6	Amplitude	2	12
242	11	75	Amplitude	2	12
243	13	74	Amplitude	2	12
244	15	16	Amplitude	2	12
245	17	80	Amplitude	2	12
246	19	9	Amplitude	2	12
247	21	2	Amplitude	2	12
248	23	88	Amplitude	2	12
249	25	98	Amplitude	2	12
250	27	27	Amplitude	2	12
251	29	53	Amplitude	2	12
252	31	94	Amplitude	2	12
253	5	19	Amplitude	1	13
254	7	37	Amplitude	1	13
255	9	35	Amplitude	1	13
256	11	37	Amplitude	1	13
257	13	4	Amplitude	1	13
258	15	72	Amplitude	1	13
259	17	91	Amplitude	1	13
260	19	46	Amplitude	1	13
261	21	46	Amplitude	1	13
262	23	41	Amplitude	1	13
263	25	54	Amplitude	1	13
264	27	8	Amplitude	1	13
265	29	99	Amplitude	1	13
266	31	31	Amplitude	1	13
267	5	20	Amplitude	2	13
268	7	80	Amplitude	2	13
269	9	14	Amplitude	2	13
270	11	29	Amplitude	2	13
271	13	83	Amplitude	2	13
272	15	87	Amplitude	2	13
273	17	92	Amplitude	2	13
274	19	74	Amplitude	2	13
275	21	34	Amplitude	2	13
276	23	89	Amplitude	2	13
277	25	44	Amplitude	2	13
278	27	70	Amplitude	2	13
279	29	45	Amplitude	2	13
280	31	47	Amplitude	2	13
281	5	28	Amplitude	1	14
282	7	12	Amplitude	1	14
283	9	22	Amplitude	1	14
284	11	65	Amplitude	1	14
285	13	83	Amplitude	1	14
286	15	9	Amplitude	1	14
287	17	26	Amplitude	1	14
288	19	7	Amplitude	1	14
289	21	21	Amplitude	1	14
290	23	60	Amplitude	1	14
291	25	81	Amplitude	1	14
292	27	88	Amplitude	1	14
293	29	58	Amplitude	1	14
294	31	15	Amplitude	1	14
295	5	77	Amplitude	2	14
296	7	28	Amplitude	2	14
297	9	40	Amplitude	2	14
298	11	10	Amplitude	2	14
299	13	0	Amplitude	2	14
300	15	57	Amplitude	2	14
301	17	90	Amplitude	2	14
302	19	93	Amplitude	2	14
303	21	45	Amplitude	2	14
304	23	3	Amplitude	2	14
305	25	76	Amplitude	2	14
306	27	66	Amplitude	2	14
307	29	38	Amplitude	2	14
308	31	81	Amplitude	2	14
\.


--
-- Name: auteur_id_auteur_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auteur_id_auteur_seq', 6, true);


--
-- Name: commentaire_id_commentaire_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.commentaire_id_commentaire_seq', 14, true);


--
-- Name: graphique_id_graph_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.graphique_id_graph_seq', 14, true);


--
-- Name: valeur_type1_id_valeur_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.valeur_type1_id_valeur_seq', 308, true);


--
-- Name: valeur_type2_id_valeur_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.valeur_type2_id_valeur_seq', 308, true);


--
-- Name: auteur auteur_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auteur
    ADD CONSTRAINT auteur_pkey PRIMARY KEY (id_auteur);


--
-- Name: commentaire commentaire_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.commentaire
    ADD CONSTRAINT commentaire_pkey PRIMARY KEY (id_commentaire);


--
-- Name: graphique graphique_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.graphique
    ADD CONSTRAINT graphique_pkey PRIMARY KEY (id_graph);


--
-- Name: valeur_type1 valeur_type1_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.valeur_type1
    ADD CONSTRAINT valeur_type1_pkey PRIMARY KEY (id_valeur);


--
-- Name: valeur_type2 valeur_type2_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.valeur_type2
    ADD CONSTRAINT valeur_type2_pkey PRIMARY KEY (id_valeur);


--
-- Name: commentaire_id_graph_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX commentaire_id_graph_id ON public.commentaire USING btree (id_graph_id);


--
-- Name: graphique_id_auteur_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX graphique_id_auteur_id ON public.graphique USING btree (id_auteur_id);


--
-- Name: valeur_type1_id_graph_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX valeur_type1_id_graph_id ON public.valeur_type1 USING btree (id_graph_id);


--
-- Name: valeur_type2_id_graph_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX valeur_type2_id_graph_id ON public.valeur_type2 USING btree (id_graph_id);


--
-- Name: commentaire commentaire_id_graph_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.commentaire
    ADD CONSTRAINT commentaire_id_graph_id_fkey FOREIGN KEY (id_graph_id) REFERENCES public.graphique(id_graph);


--
-- Name: graphique graphique_id_auteur_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.graphique
    ADD CONSTRAINT graphique_id_auteur_id_fkey FOREIGN KEY (id_auteur_id) REFERENCES public.auteur(id_auteur);


--
-- Name: valeur_type1 valeur_type1_id_graph_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.valeur_type1
    ADD CONSTRAINT valeur_type1_id_graph_id_fkey FOREIGN KEY (id_graph_id) REFERENCES public.graphique(id_graph);


--
-- Name: valeur_type2 valeur_type2_id_graph_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.valeur_type2
    ADD CONSTRAINT valeur_type2_id_graph_id_fkey FOREIGN KEY (id_graph_id) REFERENCES public.graphique(id_graph);


--
-- PostgreSQL database dump complete
--

