import modelisation
# import du fichier insert qui permet d'insert une df dans ma bdd
import insert
# import du fichier de sauvegarde et de backup auto de ma bdd
# import db_manager
import numpy as np
import main
import pandas as pd
from functools import wraps
from PyQt5.QtWidgets import (QApplication, QVBoxLayout, QHBoxLayout, QWidget,
                             QPushButton, QCheckBox, QLabel, QLineEdit,
                             QMessageBox, QInputDialog, QGridLayout, QComboBox,
                             QLCDNumber, QShortcut, QFrame, QFormLayout)

from pyqtgraph.Qt import QtGui, QtCore
from PyQt5.QtGui import QIcon, QPixmap, QIntValidator, QDoubleValidator, QSplitter
from PyQt5.QtCore import QTimer, Qt
import pyqtgraph as pg
import pathlib
from datetime import date, datetime
from random import randint
import os
import sys

# Dernière date de modification`
__version__ = "2020.09"

__author__ = "Lee-Roy Mannier"

class Window2(QWidget):                           # <===
    def __init__(self, parent=None):
        super(Window2, self).__init__(parent)     
        # Permet de récupérer le sépareteur selon l'os

        # Background de couleur Gris
        self.setStyleSheet("background-color:white;")

        # Récupération du chemin d'accès au dossier 'icon'
        # Titre de la fenêtre
        self.setWindowTitle("Analyse     v" + __version__)

        # appel des fonctions
        # fonction permettant de mettre une icon à la fenêtre
        self.setup()
        self.action_button()
    def setup(self):
        self.vboxGeneral = QVBoxLayout()
        self.hboxGeneral = QHBoxLayout()

        vbox1 = QVBoxLayout()
        hbox1 = QHBoxLayout()

        hboxselecteur = QHBoxLayout()
        # Ajout d'un widget déroulant
        self.selecteur = QComboBox()
        # self.selecteur.setFixedSize(300,300)
        self.selecteur.setFixedWidth(300)
        self.selecteur.setFixedHeight(30)
        # Ajout des items dans ce widget déroulant
        self.selecteur.addItem("Analyse")
        self.selecteur.addItem("Transformation de Fourier")
        self.selecteur.addItem("Relaxation Exponentielle")
        self.selecteur.addItem("Recherche des demi-Gaussienne")

        hboxselecteur.addWidget(self.selecteur)
        hboxselecteur.addStretch()        
        self.vboxGeneral.addLayout(hboxselecteur)
        
        self.Image_charg1 = pg.plot()
        # Je définie pa taille
        self.Image_charg1.resize(1000, 500)
        self.Image_charg1.showGrid(True, True)

        self.Image_charg2 = pg.plot()
        self.Image_charg2.resize(1000, 200)
        self.Image_charg2.showGrid(True, True)

        self.splitter1 = QSplitter(Qt.Vertical)
        self.splitter1.addWidget(self.Image_charg1)
        self.splitter1.addWidget(self.Image_charg2)

        self.vboxGeneral.addWidget(self.splitter1)

        self.fichier = QLabel("Nom du fichier .txt")
        self.edit_fichier = QLineEdit('test23')

        self.name_scan = QLabel("Le numéro du scan")
        validator1 = QIntValidator()
        self.nb_scan = QLineEdit()
        self.nb_scan.setValidator(validator1)

        self.type_nom1 = QLabel("Entrer le type 1")
        self.type1 = QLineEdit()
        self.type_nom2 = QLabel("Entrer le type 2")
        self.type2= QLineEdit()



        self.chargerButton = QPushButton("CHARGER")
        self.analyseButton = QPushButton("ANALYSER")
        self.quitterbutton = QPushButton("QUITTER")
        hbox1.addWidget(self.fichier)
        hbox1.addWidget(self.edit_fichier)
        hbox1.addWidget(self.name_scan)
        hbox1.addWidget(self.nb_scan)
        hbox1.addWidget(self.type_nom1)
        hbox1.addWidget(self.type1)
        hbox1.addWidget(self.type_nom2)
        hbox1.addWidget(self.type2)
        hbox1.addWidget(self.chargerButton)
        hbox1.addWidget(self.analyseButton)
        hbox1.addWidget(self.quitterbutton)

        self.hboxGeneral.addLayout(hbox1)
        self.vboxGeneral.addLayout(self.hboxGeneral)
        self.setLayout(self.vboxGeneral)

    def action_button(self):
        self.chargerButton.clicked.connect(self.charger)
            #self.analyseButton.connect()
        self.quitterbutton.clicked.connect(self.quitter)
    
    def chargement_df_text(self, fichier_text, nb_scan, type_graph1, type_graph2=None):
        #TODO If scan ==None charger tout les scans
        
        df = pd.read_csv(fichier_text, skiprows=12, sep='\t')
        df = df[df["nb_graph"] == nb_scan]
        
        liste_position = []
        liste_abscisse = []

        liste_position2 = []
        liste_abscisse2 = []

        if type_graph1 == "Reel" or type_graph1 == "Phase":
            df_position = df.loc[:, ["# position"]]
            df_colonne = df.loc[:, ["x"]]
            df_liste_tmp1 = df_position.values.tolist()
            df_liste_tmp2 = df_colonne.values.tolist()

            for i in df_liste_tmp1:
                for y in i:
                    liste_position.append(y)

            for i in df_liste_tmp2:
                for y in i:
                    liste_abscisse.append(y)

            self.Image_charg1.plot(liste_position, liste_abscisse)

        if type_graph2 == "Imaginaire" or type_graph2 == "Amplitude":
            df_position2 = df.loc[:, ["# position"]]
            df_colonne2 = df.loc[:, ["y"]]
            df_liste_tmp1 = df_position2.values.tolist()
            df_liste_tmp2 = df_colonne2.values.tolist()

            for i in df_liste_tmp1:
                for y in i:
                    liste_position2.append(y)

            for i in df_liste_tmp2:
                for y in i:
                    liste_abscisse2.append(y)
            self.Image_charg2.plot(liste_position2, liste_abscisse2)

    
    def charger(self):
        self.charg_fichier = str(self.edit_fichier.text()) + ".txt"
        self.nb_scan_display = int(self.nb_scan.text())
        self.type_graph = str(self.type1.text())
        self.type_graph2 = str(self.type2.text())
        self.chargement_df_text("/Users/lmannier/Desktop/Projet Experience Qt/sauvegarde/"+self.charg_fichier, self.nb_scan_display, self.type_graph, self.type_graph2)

    def quitter(self):
        self.w = main.Display()
        self.w.show()
        self.close()
