"""
.########..####..######...####.########....###....########.
.##.....##..##..##....##...##..##.........##.##...##.....##
.##.....##..##..##.........##..##........##...##..##.....##
.##.....##..##..##...####..##..######...##.....##.########.
.##.....##..##..##....##...##..##.......#########.##.....##
.##.....##..##..##....##...##..##.......##.....##.##.....##
.########..####..######...####.##.......##.....##.########.

introduction:
Création d’une application avec le langage Python ayant pour but d'aider le
Dr Davide Boschetto, professeur et chercheur à l'école polytechnique de
ParisTech, à visualiser les résultats sous forme graphique d’une expérience
d’interaction entre un laser et une cible. L’application aura aussi pour but
d’analyser les résultats par la recherche automatique du signale et par
l’interpolation de ce signal à l’aide de mélange de fonctions.

L'application possèdera un champ de text qui possedera la
valeur de départ de la position du moteur, un 2eme champ
de text qui possedera la valeur de fin, un 3eme champ
de text qui possedera le pas, un 4eme champ de text prendra
le temps de deplacement du capteur, un boutton START,
un boutton STOP

TECHNOLOGIE:
    Pour ce projet on utilisera:
            Qt (PyQt5) -> Interface Graphique
            pyqtgraph  -> Graphique

La base de donnée est constituée de plusieurs tables:
    - Auteur => contient le nom des auteurs des graphiques

    - Graphique => contient les types de graphique, leurs dates et une
                   foreignkey qui pointe sur Auteur

    - Valeur_type1 => contient les valeurs des X et Y, leurs types1, le nombre
                      de scan et une foreignkey qui pointe sur Graphique

    - Valeur_type2 => contient les valeurs des X et Y, leurs types2, le nombre
                      de scan et une foreignkey qui pointe sur Graphique

    - Commentaire => contient les commentaires des graphiques et une foreignkey
                     qui pointe sur Graphique

Qt Widgets RAPPEL:
    - QLabel        => Ecrire quelque chose
    - QLineEdit     => Zone de Texte
    - QPushButton   => Bouton
    - QComboBox     => Liste défilante
    - QCheckBox     => Switch On / Off
    - QLCDNumber    => Affiche un nombre facilement connectable
"""
# import de ma bdd modélisé
import modelisation
# import du fichier insert qui permet d'insert une df dans ma bdd
import insert
# import du fichier de sauvegarde et de backup auto de ma bdd
# import db_manager
import numpy as np
import chargement
import pandas as pd
from functools import wraps
from PyQt5.QtWidgets import (QApplication, QVBoxLayout, QHBoxLayout, QWidget,
                             QPushButton, QCheckBox, QLabel, QLineEdit,
                             QMessageBox, QInputDialog, QGridLayout, QComboBox,
                             QLCDNumber, QShortcut, QFrame, QFormLayout)

from pyqtgraph.Qt import QtGui, QtCore
from PyQt5.QtGui import QIcon, QPixmap, QIntValidator, QDoubleValidator, QSplitter
from PyQt5.QtCore import QTimer, Qt
import pyqtgraph as pg
import pathlib
from datetime import date, datetime
from random import randint
import os
import sys
import statistics

## TODO IMPORT LOGGING
# Dernière date de modification`

__version__ = "2020.09"

__author__ = "Lee-Roy Mannier"


class Display(QWidget):
    """
    ===========================================================================#
        Class General pour afficher l'application ainsi que la mise            #
        en place de differente variable de gestion                             #
    ===========================================================================#
    """

    def __init__(self, parent=None):
        super(Display, self).__init__(parent)
        # Permet de récupérer le chemin du fichier et le sépareur selon de l'os
        self.pathfile = pathlib.Path(__file__)
        # Permet de récupérer le sépareteur selon l'os
        sepa = os.sep

        # Background de couleur Gris
        self.setStyleSheet("background-color:gray;")

        # Récupération du chemin d'accès au dossier 'icon'
        self.icon = str(self.pathfile.parent) + sepa + 'icon' + sepa
        # Titre de la fenêtre
        self.setWindowTitle("Visualisation     v" + __version__)

        # appel des fonctions
        self.setup()
        self.action_button()
        self.shortcut()
        self.preparation_moteur()
        # fonction permettant de mettre une icon à la fenêtre
        self.setWindowIcon(QIcon(self.icon + 'DIGIFAB.png'))

    # Fonction de mise en place des gadgets de la fenêtre

    def setup(self):
        """
        =======================================================================#
            Mise en place des Widgets                                          #
        =======================================================================#
        """
        # récupération des icones de mon slider
        TogOff = self.icon + 'Toggle_Off.png'
        TogOn = self.icon + 'Toggle_On.png'

        TogOff = pathlib.Path(TogOff)
        TogOff = pathlib.PurePosixPath(TogOff)
        TogOn = pathlib.Path(TogOn)
        TogOn = pathlib.PurePosixPath(TogOn)

        # Feuille de style de mes boutons
        self.setStyleSheet(
            "QCheckBox::indicator{width: 30px;height: 30px;}"
            "QCheckBox::indicator:unchecked { image : url(%s);}"
            "QCheckBox::indicator:checked { image:  url(%s);}"
            "QCheckBox{font :10pt;}" %
            (TogOff, TogOn))

        # Enboitement général des gadgets
        self.vboxGeneral = QVBoxLayout()
        self.hboxGeneral = QHBoxLayout()

        vbox1 = QVBoxLayout()
        hbox1 = QHBoxLayout()

        self.graph2 = False
        # Variable d'activation de l'auto save
        self.autosauvegarde = False
        # Liste qui permettra de récupérer les valeurs de base de mes widgets
        self.edits = []
        # Variable d'activation d'arret scan
        self.arretscan = False
        # Dico qui contiendra les valeurs de X, Y des graphiques, le type de
        # de graphique généré, le nombre de scan reçu
        self.dico_valeur1 = {'position': [],
                             'x': [],
                             "type_graph": [],
                             "nb_scan": [],
                             "id_graph_id": []}

        self.dico_valeur2 = {'position': [],
                             'y': [],
                             "type_graph": [],
                             "nb_scan": [],
                             "id_graph_id": []}
        self.tmp = pd.DataFrame()
        self.tmp2 = pd.DataFrame()

        self.df_text = pd.DataFrame()

        # Variable d'incrementation
        self.incMoteur = 0
        # Compteur de points par graphique
        self.count = 0
        # Compteur du nombre de graphique
        self.count_graph = 0
        # permettra l'incrementation (1) a coté de mon fichier si il existe
        # deja
        self.nbfichier = 0

        self.insert_bdd = False

        # Creation d'un label "Start"
        self.nomStart = QLabel("Start")
        self.nomStart.setStyleSheet("background-color:green")
        # Que des valeurs en Integer
        self.locale = QtCore.QLocale("en")
        validator1 = QDoubleValidator(-100, 100, 2)
        # Creation d'une zone de texte
        self.RangeStart = QLineEdit('-10.0')
        validator1.setLocale(self.locale)
        self.RangeStart.setValidator(validator1)
        self.edits.append(self.RangeStart)

        # J'ajoute mes widget a ma boite
        hbox1.addWidget(self.nomStart)
        hbox1.addWidget(self.RangeStart)

        # J'ajoute ma boite a ma boite principale
        vbox1.addLayout(hbox1)

        hbox2 = QHBoxLayout()

        self.nomStop = QLabel("Stop ")
        self.nomStop.setStyleSheet("background-color:red")
        validator2 = QDoubleValidator(-100, 100, 2)
        self.RangeStop = QLineEdit('15.0')
        validator2.setLocale(self.locale)
        self.RangeStop.setValidator(validator2)
        self.edits.append(self.RangeStop)

        hbox2.addWidget(self.nomStop)
        hbox2.addWidget(self.RangeStop)

        vbox1.addLayout(hbox2)

        hbox3 = QHBoxLayout()

        self.nomPas = QLabel("Pas (ps) ")
        validator3 = QDoubleValidator(-100, 100, 2)
        self.RangePas = QLineEdit('1.0')
        validator3.setLocale(self.locale)
        self.RangePas.setValidator(validator3)
        self.edits.append(self.RangePas)

        self.nombre_de_pas = None

        hbox3.addWidget(self.nomPas)
        hbox3.addWidget(self.RangePas)

        vbox1.addLayout(hbox3)

        self.hboxGeneral.addLayout(vbox1)
        self.hboxGeneral.addStretch()

        hbox4 = QHBoxLayout()
        vbox2 = QVBoxLayout()

        self.nomAttente = QLabel("temps d'attente (s)")
        validator4 = QDoubleValidator(-100, 100, 2)
        self.RangeAttente = QLineEdit('1.0')
        self.RangeAttente.setValidator(validator4)
        validator4.setLocale(self.locale)
        self.edits.append(self.RangeAttente)

        hbox4.addWidget(self.nomAttente)
        hbox4.addWidget(self.RangeAttente)

        vbox2.addLayout(hbox4)

        hbox5 = QHBoxLayout()

        self.nomScan = QLabel("NB Scan            ")
        validator5 = QIntValidator()
        self.RangeScan = QLineEdit('4')
        self.RangeScan.setValidator(validator5)
        self.edits.append(self.RangeScan)

        hbox5.addWidget(self.nomScan)
        hbox5.addWidget(self.RangeScan)

        vbox2.addLayout(hbox5)

        hbox6 = QHBoxLayout()

        self.nomInitial = QLabel("t0 (ps)    ")
        validator6 = QDoubleValidator(0, 1000, 2)
        self.RangeInitial = QLineEdit('100.0')
        self.positionactuelle = float(self.RangeInitial.text())
        self.RangeInitial.setValidator(validator6)
        validator6.setLocale(self.locale)
        self.edits.append(self.RangeInitial)

        hbox6.addWidget(self.nomInitial)
        hbox6.addWidget(self.RangeInitial)

        vbox2.addLayout(hbox6)

        self.hboxGeneral.addLayout(vbox2)
        # self.hboxGeneral.addStretch()
        hboxselecteur = QHBoxLayout()
        # Ajout d'un widget déroulant
        
        self.selecteur1 = QComboBox()
        # self.selecteur1.setFixedSize(300,300)
        self.selecteur1.setFixedWidth(300)
        self.selecteur1.setFixedHeight(30)
        self.selecteur1.addItem("Chargement")
        self.selecteur1.addItem("Charger une experience")

        self.img = QLabel()
        # Ajout d'une image avec le répértoire de l'image
        self.img.setPixmap(QPixmap(self.icon + 'digilogo.png'))

        hboxselecteur.addWidget(self.selecteur1)
        hboxselecteur.addStretch()
        hboxselecteur.addWidget(self.img)
        self.vboxGeneral.addLayout(hboxselecteur)
        # Creation d'un objet graphique vide
        # pg.setConfigOption('background', '#2A52BE')

        pg.setConfigOption('background', 'k')
        """
        ===============================/\==============================#
        Posibilité de mettre un fond blanc ou noir pour les graphiques #
                background w => white                                  #
                background k => black                                  #
        ===============================/\==============================#
        """
        self.ImageW1 = pg.plot()
        # Je définie pa taille
        self.ImageW1.resize(1000, 500)
        self.ImageW1.showGrid(True, True)

        self.ImageW2 = pg.plot()
        self.ImageW2.resize(1000, 200)
        self.ImageW2.showGrid(True, True)

        self.splitter1 = QSplitter(Qt.Vertical)
        self.splitter1.addWidget(self.ImageW1)
        self.splitter1.addWidget(self.ImageW2)

        self.vboxGeneral.addWidget(self.splitter1)

        self.hboxGeneral.addLayout(hbox2)
        self.vboxGeneral.addLayout(self.hboxGeneral)

        vbox3 = QVBoxLayout()
        # Séparation de widget pour avoir de
        self.hboxGeneral.addStretch()
        # Permet un espacement entre block
        self.StartButton = QPushButton("START")
        self.ArretButton = QPushButton("ARRET APRES LE SCAN")
        self.StopButton = QPushButton("STOP - RESET")
        self.ResetButton = QPushButton("RESET GRAPHIQUE")
        self.QuitterButton = QPushButton("QUITTER")

        vbox3.addWidget(self.StartButton)
        vbox3.addWidget(self.ArretButton)
        vbox3.addWidget(self.ResetButton)
        vbox3.addWidget(self.StopButton)
        vbox3.addWidget(self.QuitterButton)

        self.hboxGeneral.addLayout(vbox3)

        self.hboxGeneral.addStretch()

        vbox4 = QVBoxLayout()
        hboxCheck = QHBoxLayout()
        self.CommentaireButton = QPushButton(
            "DESCRIPTION DE L'EXPERIENCE / COMMENTAIRE", self)
        # Creation d'un objet checkbox True / False
        self.checkBoxAutoSave = QCheckBox('Auto sauvegarde', self)
        # Je défini la position de départ a False
        self.checkBoxAutoSave.setChecked(False)

        # self.ReelImaginaire = QLabel("partie imaginaire - reel")
        self.switch_name1 = QLabel("partie phase - amplitude")
        # False = Partie Phase Amplitude True = Imaginaire Reel
        self.check_Partie = QCheckBox("")
        self.switch_name2 = QLabel("partie imaginaire - reel")
        self.check_Partie.setChecked(False)

        vbox4.addWidget(self.CommentaireButton)
        vbox4.addWidget(self.checkBoxAutoSave)
        hboxCheck.addWidget(self.switch_name1)
        hboxCheck.addWidget(self.check_Partie)
        hboxCheck.addWidget(self.switch_name2)
        vbox4.addLayout(hboxCheck)
        vbox5 = QVBoxLayout()
        self.buttonGraph2 = QCheckBox("Affichage graphique 2")
        self.buttonGraph2.setChecked(False)
        self.gestionMoteur = QLabel("------------Gestion Moteur-----------")
        self.jumpLine = QLabel()
        self.comMoteur = QPushButton("Communication Moteur")

        hboxMoteur = QHBoxLayout()
        # Widget d'affichage LCD d'un nombre
        self.moteurDigit = QLCDNumber()
        self.plusMoteur = QPushButton("+")
        self.moinMoteur = QPushButton("-")
        self.auteurLabel = QLabel("Auteur de l'experience")
        self.auteur = QLineEdit()
        hboxMoteur.addWidget(self.plusMoteur)
        hboxMoteur.addWidget(self.moteurDigit)
        hboxMoteur.addWidget(self.moinMoteur)
        vbox5.addWidget(self.buttonGraph2)
        vbox5.addWidget(self.auteurLabel)
        vbox5.addWidget(self.auteur)
        vbox5.addWidget(self.jumpLine)
        vbox5.addWidget(self.gestionMoteur)
        vbox5.addWidget(self.jumpLine)
        vbox5.addWidget(self.comMoteur)
        vbox5.addWidget(self.jumpLine)

        vbox5.addLayout(hboxMoteur)
        self.hboxGeneral.addLayout(vbox4)
        self.hboxGeneral.addStretch()

        self.hboxGeneral.addLayout(vbox5)
        self.setLayout(self.vboxGeneral)
        # TODO if self.buttonGraph2:
        # self.ImageW3 = pg.plot()
        # self.ImageW3.resize(1000, 500)
        # self.ImageW3.showGrid(True, True)

        # self.ImageW4 = pg.plot()
        # self.ImageW4.resize(1000, 500)
        # self.ImageW4.showGrid(True, True)
        # self.vboxGeneral.addWidget(self.ImageW3)
        # self.vboxGeneral.addWidget(self.ImageW4)

    def switch_type(self, bool_switch):
        if bool_switch is True:
            self.type1 = "Reel"
            self.type2 = "Imaginaire"
        else:
            self.type1 = "Phase"
            self.type2 = "Amplitude"
        return self.type1, self.type2

    def action_button(self):
        """
        =======================================================================#
            #
            Fonction qui lie mes objets à leurs actions (les fonctions)
        =======================================================================#
        """
        # Si Start est cliqué alors j'appel la fonction graphique
        self.StartButton.clicked.connect(self.saveBDD)
        self.StartButton.clicked.connect(self.graphique)

        # Si Arret est cliqué j'arrete après mon scan
        self.ArretButton.clicked.connect(self.arret_scan)
        # Si Stop est cliqué j'appel la fonction all_clear
        self.ResetButton.clicked.connect(self.reset_graph)
        self.StopButton.clicked.connect(self.all_clear)
        # Si Commentaire est cliqué, ouvre un pop up pour entrer des
        # commentaires
        self.CommentaireButton.clicked.connect(self.commentaire)
        # Si Quitter est cliquer, quitte l'application
        self.QuitterButton.clicked.connect(self.quitter)
        # Si l'autosave est switch en True, auto save les données
        self.checkBoxAutoSave.stateChanged.connect(self.auto_save)
        # Si + est cliqué, incremente le moteur de 1
        self.moinMoteur.clicked.connect(self.decrement)
        # Si - est cliqué désincremente le moteur de 1
        self.plusMoteur.clicked.connect(self.increment)
        # Permet de communiquer avec le moteur
        self.comMoteur.clicked.connect(self.communication_moteur)
        #
        self.buttonGraph2.stateChanged.connect(self.graphique2Act)

        self.selecteur1.currentTextChanged.connect(self.cherche_plot)

    def shortcut(self):
        """
        =======================================================================#
            Fonction de raccourcie clavier                                     #
        =======================================================================#
        """
        # Raccourcie + pour incrementer
        self.shortcutPlus = QShortcut(QtGui.QKeySequence("+"), self)
        # Connection a la fonction increment
        self.shortcutPlus.activated.connect(self.increment)
        # Raccourcie - pour désincrementer
        self.shortcutMoin = QShortcut(QtGui.QKeySequence("-"), self)
        # Connection a la fonction decrement
        self.shortcutMoin.activated.connect(self.decrement)

    def increment(self):
        """
        =======================================================================#
            Fonction incrementation qui augmente de 1 le digit du moteur       #
        =======================================================================#
        """
        self.incMoteur += 1
        # Affichage de incMoteur sur le widget LCD
        self.moteurDigit.display(self.incMoteur)
        print(float(self.RangeStart.text()))
        print(float(self.RangeStop.text()))
        print(float(self.RangePas.text()))


    def decrement(self):
        """
        =======================================================================#
            Fonction désincrementation qui augmente de 1 le digit du moteur    #
        =======================================================================#
        """
        self.incMoteur -= 1
        self.moteurDigit.display(self.incMoteur)

    def save_metadata(self, adresse, action, table):
        """
        =======================================================================#
            Fonction de sauvegarde de mes metadata dans un fichier text        #
            il sauvegardera les actions faites ainsi que la date               #
        =======================================================================#
        """
        fichier = open(adresse, 'a')
        temps = datetime.now()
        temps = temps.strftime("%Y-%m-%d %H:%M:%S")
        fichier.write(action + ' ' + table + ' ' + temps + '\n')
        fichier.close()

    def saveBDD(self):
        self.auteur_text = self.auteur.text()
        self.type1, self.type2 = self.switch_type(
            self.check_Partie.isChecked())

        if self.auteur_text == "":
            self.auteur_text = "unknow"
        else:
            self.auteur_text = self.auteur_text
        
        if self.insert_bdd:
            modelisation.insert_auteur(self.auteur_text)
            modelisation.insert_graphique(
                str(self.type1 + "/" + self.type2), self.auteur_text)

            self.save_metadata(
                "metadata/metadata.txt",
                "INSERT INTO",
                "Auteur")
            self.save_metadata(
                "metadata/metadata.txt",
                "INSERT INTO",
                "Graphique")
        else:
            print("Pas de bdd active")
            self.save_metadata(
                "metadata/metadata.txt",
                "NO DATA BASE ACTIVATED",
                "Auteur")
            self.save_metadata(
                "metadata/metadata.txt",
                "NO DATA BASE ACTIVATED",
                "Graphique")
    
    def graphique2Act(self):
        """
         ======================================================================#
            TODO affichage des 2 autres graphiques                             #
        =======================================================================#
        """
        boxg = QVBoxLayout()
        if self.buttonGraph2.isChecked():
            self.graph2 = True
            self.ImageW3 = pg.plot()
            self.ImageW3.resize(1000, 500)
            self.ImageW3.showGrid(True, True)
            self.ImageW4 = pg.plot()
            self.ImageW4.resize(1000, 500)
            self.ImageW4.showGrid(True, True)
            boxg.addWidget(self.ImageW3)
            boxg.addWidget(self.ImageW4)

        else:
            self.graph2 = False
            # TODO Faire en sorte d'enlever le grpah 2

    def demande_position_moteur(self):
        return self.positionactuelle

    def deplacer_moteur(self, position):
        self.positionactuelle = position

    def detection(self):
        return list(np.random.rand(2))

    def preparation_moteur(self):
        """
        =======================================================================#
            Fonction de préparation de mon moteur                              #
        =======================================================================#
        """
        self.RangeStartFloat = float(self.RangeStart.text())
        self.RangeStopFloat = float(self.RangeStop.text())
        self.RangePasFloat = float(self.RangePas.text())

        # nombre_de_pas devrait être updaté chaque fois que RangeStart,
        # RangeStop ou RnagePas sont changés
        # function indépendante + clicked connect ?
        self.nombre_de_pas = round(
            (self.RangeStopFloat - self.RangeStartFloat) / self.RangePasFloat)
        return self.nombre_de_pas
    
    
    def preparation_valeur(self, dico, tuple_colonne, *argv):
        """
        =======================================================================#
                Fonction de preparation de mon dico de valeur                  #
        =======================================================================#
        """
        dico[tuple_colonne[0]].append(argv[0])
        dico[tuple_colonne[1]].append(argv[1])
        dico[tuple_colonne[2]].append(argv[2])
        dico[tuple_colonne[3]].append(argv[3] + 1)
        dico[tuple_colonne[4]].append(argv[4])
    
    def mean_liste_valeur(self,liste_final):
        ## TODO DATAFRAME 
        self.listefinal = []
        self.liste_moyenne = []
        self.listetmp = []

        for x in range(len(liste_final)):
            for i in range(len(liste_final[0])):
                for y in liste_final:
                    self.listetmp.append(y[i])
                self.listefinal.append(self.listetmp)
                self.listetmp = []

        for moyenne in self.listefinal:
            self.liste_moyenne.append(statistics.mean(moyenne))
        return self.liste_moyenne

    def graphique(self):
        """
        =======================================================================#
            #
            Fonction qui genère mes graphique (selon le nombre de scan voulu)
            il appel de façon récursive ma fonction update_plot_data ce qui    #
            permet de générer un graphique en temps réel                       #
        =======================================================================#
        """
        self.pas = self.preparation_moteur()
        if self.insert_bdd:
            id_graph_objet = modelisation.Graphique.select().order_by(
                modelisation.Graphique.id_graph.desc()).get()
            # Récuperation du dernier id_graph de ma table grace à mon objet
            self.id_graphique = id_graph_objet.id_graph
        else:
            self.id_graphique = 1
        self.dico_valeur1 = {'position': [],
                             'x': [],
                             "type_graph": [],
                             "nb_scan": [],
                             "id_graph_id": []}

        self.dico_valeur2 = {'position': [],
                             'y': [],
                             "type_graph": [],
                             "nb_scan": [],
                             "id_graph_id": []}
    
        self.moteur_ajout = []

        self.pos = float(self.RangeStart.text())
        self.x, self.y = self.detection()
        self.rangeAttente = float(self.RangeAttente.text())
        self.scan = int(self.RangeScan.text())
        
        self.preparation_valeur(
                self.dico_valeur1,
                tuple(
                    self.dico_valeur1.keys()),
                self.pos,
                self.x,
                self.type1,
                self.count_graph,
                self.id_graphique)

        self.preparation_valeur(
                self.dico_valeur2,
                tuple(
                    self.dico_valeur2.keys()),
                self.pos,
                self.y,
                self.type2,
                self.count_graph,
                self.id_graphique)
        listeSymbol = [
            "o",
            "+",
            "x",
            "d",
            "star",
            "t",
            "t1",
            "t2",
            "t3",
            "s",
            "p",
            "o",
            "+",
            "x",
            "d",
            "star",
            "t",
            "t1",
            "t2",
            "t3",
            "s",
            "p"]
        listColor = ["r", "w", "y", "b", "c", "m", "g", "m","c","b","y","g",
                     "w","r", "b","m","g","c", "m", "g", "m","c"]
        symbol = listeSymbol[self.count_graph]
        color = listColor[self.count_graph]
        self.ImageW1.addLegend()
        self.ImageW2.addLegend()
        symbol = listeSymbol[self.count_graph]
        self.ImageW1.addLegend()
        self.ImageW2.addLegend()
        if self.count_graph < 1:
            self.ImageW1.setTitle("Amplitude graphique 1")
            self.ImageW2.setTitle("Phase graphique 1")

            self.data_line1 = self.ImageW1.plot(
                self.dico_valeur1["position"],
                self.dico_valeur1["x"],
                pen=color,
                symbol=symbol,
                name="scan " + str(
                    self.count_graph + 1))
            self.data_line2 = self.ImageW2.plot(
                self.dico_valeur2["position"],
                self.dico_valeur2["y"],
                pen=color,
                symbol=symbol,
                name="scan " + str(
                    self.count_graph + 1))

        else:
            self.data_line1 = self.ImageW1.plot(
                self.dico_valeur1["position"],
                self.dico_valeur1["x"],
                pen=color,
                symbol=symbol,
                name="scan " + str(
                    self.count_graph + 1))

            self.data_line2 = self.ImageW2.plot(
                self.dico_valeur2["position"],
                self.dico_valeur2["y"],
                pen=color,
                symbol=symbol,
                name="scan " + str(
                    self.count_graph + 1))

        if self.count_graph < self.scan:
            self.timer = QTimer()
            self.timer.setInterval(self.rangeAttente * 1000)
            self.timer.timeout.connect(self.update_plot_data)
            self.timer.start()

    def update_plot_data(self):
        """
        =======================================================================#
            Fonction qui mets a jour mes données du graphique                  #
        =======================================================================#
        """
        # Objet peewee de ma table Graphique qui contient le resultat de ma
        # requete
        if self.count < self.nombre_de_pas:
            self.moteurDigit.display(self.pos)
            self.pos = self.pos + float(self.RangePas.text())
            self.deplacer_moteur(self.pos)
            self.pos = self.demande_position_moteur()
            self.x, self.y = self.detection()
            
            self.preparation_valeur(
                self.dico_valeur1,
                tuple(
                    self.dico_valeur1.keys()),
                self.pos,
                self.x,
                self.type1,
                self.count_graph,
                self.id_graphique)

            self.preparation_valeur(
                self.dico_valeur2,
                tuple(
                    self.dico_valeur2.keys()),
                self.pos,
                self.y,
                self.type2,
                self.count_graph,
                self.id_graphique)

            self.data_line1.setData(
                self.dico_valeur1["position"],
                self.dico_valeur1["x"])
            self.data_line2.setData(
                self.dico_valeur2["position"],
                self.dico_valeur2["y"])

            self.count += 1
        else:

            self.count = 0
            self.count_graph += 1
            self.dataValeur1 = pd.DataFrame(self.dico_valeur1)
            self.dataValeur2 = pd.DataFrame(self.dico_valeur2)

            self.tmp = pd.concat([self.tmp, self.dataValeur1],ignore_index=True)
            self.tmp2 = pd.concat([self.tmp2, self.dataValeur2],ignore_index=True)

            if self.autosauvegarde:
                # concatenation de 2 dataframe pour les mettre dans le fichier
                # text

                self.dfConcat = pd.concat([self.tmp,
                                           self.tmp2], axis=1)
                np.savetxt(
                    self.fichier_autosave,
                    self.dfConcat,
                    fmt='%s',
                    delimiter="\t",
                    footer="\n")

            if self.count_graph == int(self.RangeScan.text()):
                # concatenation de 2 dataframe pour les mettre dans le fichier
                # text
                self.dfConcat = pd.concat([self.tmp,
                                           self.tmp2], axis=1)
                
                if self.insert_bdd:
                    # fct insert de mes dataframes dans ma bdd
                    insert.insert_de_masse(
                        self.tmp, "Valeur_type1", insert.con)
                    insert.insert_de_masse(
                        self.tmp2, "Valeur_type2", insert.con)
                    # Je save mes metadata dans mon fichier text
                    self.save_metadata(
                        "metadata/metadata.txt",
                        "INSERT INTO",
                        "Valeur_type1")
                    self.save_metadata(
                        "metadata/metadata.txt",
                        "INSERT INTO",
                        "Valeur_type2")
                else:
                    self.save_metadata(
                        "metadata/metadata.txt",
                        "NO DATA BASE ACTIVATED",
                        "Valeur_type1")
                    self.save_metadata(
                        "metadata/metadata.txt",
                        "NO DATA BASE ACTIVATED",
                        "Valeur_type2")
                    print("Pas de bdd active")

                self.timer.stop()
                self.save(self.dfConcat)

            elif self.arretscan:
                # Transforme mon dico de valeur en dataframe
                self.dfConcat = pd.concat([self.tmp,
                                           self.tmp2], axis=1)
                print(self.dfConcat)
                if self.insert_bdd:
                    # fct insert de mes dataframes dans ma bdd
                    insert.insert_de_masse(
                        self.tmp, "Valeur_type1", insert.con)
                    insert.insert_de_masse(
                        self.tmp2, "Valeur_type2", insert.con)
                    # Je save mes metadata dans mon fichier text
                    self.save_metadata(
                        "metadata/metadata.txt",
                        "INSERT INTO",
                        "Valeur_type1")
                    self.save_metadata(
                        "metadata/metadata.txt",
                        "INSERT INTO",
                        "Valeur_type2")
                else:
                    print("pas de bdd active")
           
                self.timer.stop()
                self.save(self.dfConcat)
            else:
                self.graphique()

    def all_clear(self):
        """
        =======================================================================#
            Fonction qui permet de clear toute l'appli lorsqu'on appuie sur le #
            Bouton STOP                                                        #
        =======================================================================#
        """
        if self.count_graph > 0:
            self.timer.stop()

        for edit in self.edits:
            edit.clear()

        self.ImageW1.clear()
        self.ImageW2.clear()
        
        self.dico_valeur1 = {'position': [],
                             'x': [],
                             "type_graph": [],
                             "nb_scan": [],
                             "id_graph_id": []}

        self.dico_valeur2 = {'position': [],
                             'y': [],
                             "type_graph": [],
                             "nb_scan": [],
                             "id_graph_id": []}
        
        self.tmp = pd.DataFrame()
        self.tmp2 = pd.DataFrame()
        self.count = 0
        self.count_graph = 0
        self.arretscan = False

    def reset_graph(self):
        """
        =======================================================================#
            Fonction qui efface les graphiques actuels pour pouvoir en générer #
            des nouveaux                                                       #
        =======================================================================#
        """
        self.ImageW1.clear()
        self.ImageW2.clear()
        self.dico_valeur1 = {'position': [],
                             'x': [],
                             "type_graph": [],
                             "nb_scan": [],
                             "id_graph_id": []}

        self.dico_valeur2 = {'position': [],
                             'y': [],
                             "type_graph": [],
                             "nb_scan": [],
                             "id_graph_id": []}
        self.count = 0
        self.count_graph = 0

        #self.dataValeur1 = pd.DataFrame([])
        #self.dataValeur2 = pd.DataFrame([])
       
        self.tmp = pd.DataFrame()
        self.tmp2 = pd.DataFrame()
        self.arretscan = False

    def header_txt(self):
        """
        =======================================================================#
            Fonction qui contient le header pour les fichiers de sauvegarde    #
            TODO => mettre des argument pour modifier les points, le titre..   #
        =======================================================================#
        """
        self.header = "@ASCII_DATA_FILE"
        self.header += "\nNCurves=12"
        self.header += "\nNPoints=121"
        self.header += "\nTitle=very well reproduced with clearly enhanced oscillations when probe rotated"
        self.header += "\nSubTitle=" + str(datetime.now())
        self.header += "\nXLabel=Axe X en fs"
        self.header += "\nYLabel="
        self.header += "\nFWHM=0,000000"
        self.header += "\nfitform=0"
        self.header += "\n@END_HEADER\n"

        return self.header

    def arret_scan(self):
        """
        =======================================================================#
            Fonction qui arrete de generer un graphique apres mon scan         #
        =======================================================================#
        """
        self.arretscan = True

    def commentaire(self):
        """
        =======================================================================#
            Fonction qui permet d'ouvrir un pop pour rentrer un commentaire    #
            qui sera enregister dans notre fichier .text                       #
        =======================================================================#
        """
        self.today = date.today()
        self.text, self.ok = QInputDialog.getMultiLineText(
            self, 'getMultiLineText', 'Commentaires', 'Entrer vos commentaires')

        if self.ok and self.text:
            self.commentaires = self.text
            if self.autosauvegarde:
                self.fichier_autosave.write(self.commentaires)
                if self.insert_bdd:
                    modelisation.insert_commentaire(
                    self.commentaires, self.id_graphique)
            else:
                pass
        else:
            self.save_metadata(
                "metadata/metadata.txt",
                "NO DATA BASE ACTIVATED",
                "Commentaire")
            print("pas de bdd active")
   
    def cherche_plot(self):
        self.w = chargement.Window2()
        self.w.show()
        self.hide()
        #self.chargement_df_text("/Users/lmannier/Desktop/Projet Experience Qt/sauvegarde/testdata(1).txt", 2, "Reel")

    def save(self, dffichier):
        """
        =======================================================================#
            Fonction de sauvegarde de mes données en .txt                      #
        =======================================================================#
        """
        self.header = self.header_txt()
        self.choixCommentaire = QMessageBox.question(
            self,
            "Commentaire ?",
            "Voulez-vous mettre un commentaire ?",
            QMessageBox.Yes | QMessageBox.No)
        if self.choixCommentaire == QMessageBox.Yes:
            self.commentaire()
        else:
            self.commentaires = "Pas de commentaire"
        self.choix = QMessageBox.question(
            self,
            "SAUVEGARDER ?",
            "Voulez-vous sauvegarder ?",
            QMessageBox.Yes | QMessageBox.No)
        if self.choix == QMessageBox.Yes:
            # J'ouvre une box de type Dialog avec un titre
            fname = QtGui.QFileDialog.getSaveFileName(
                self, "Sauvegarder en TXT")
            # Je défini le nom de mon fichier a sauvegarder
            self.pathfile = os.path.dirname(str(fname[0]))
            fichier = fname[0]
            print(fichier, " sauvegarder")
            # J'ouvre mon fichier pour pouvoir écrire à l'interieur
            # Newfichier = open(fichier + ".txt", 'w')
            if os.path.isfile(
                    fichier + "(" + str(self.nbfichier) + ")" + ".txt"):
                while True:
                    self.nbfichier += 1
                    if os.path.isfile(
                            fichier + "(" + str(self.nbfichier) + ")" + ".txt"):
                        continue
                    else:
                        break

            Newfichier = open(
                fichier + "(" + str(self.nbfichier) + ")" + ".txt", 'w')
            Newfichier.write(self.header + "\n")
            Newfichier.write(self.commentaires + "\n")
            # Je sauvegarde mes donnée dans un fichier .txt
            # Une fois fini je le ferme
            np.savetxt(Newfichier, dffichier, fmt='%s', delimiter="\t", header="position\tx\ttype_graph\tnb_graph\tid_grap\tposition\ty\ttype_graph\tnb_graph\tid_graph")
            Newfichier.close()
        if self.insert_bdd:
            modelisation.insert_commentaire(
                self.commentaires, self.id_graphique)
        else:
            self.save_metadata(
                "metadata/metadata.txt",
                "NO DATA BASE ACTIVATED",
                "Commentaire")
            print("pas de bdd active")

    def auto_save(self):
        """
        =======================================================================#
            Fonction qui permet la sauvegarde automatique                      #
        =======================================================================#
        """
        self.header = self.header_txt()

        if self.checkBoxAutoSave.isChecked():
            self.autosauvegarde = True
            self.today = datetime.now().strftime('%Y-%m-%d %H:%M')

            self.fichier_autosave = open(str(self.today) + " data.txt", "w")
            self.fichier_autosave.write(self.header + "\n")
        else:
            self.autosauvegarde = False


    def communication_moteur(self):
        """
        =======================================================================#
            Fonction de communication avec le moteur                           #
        =======================================================================#
        """
        #self.BoxComMoteur, self.ok = QInputDialog.getText(
        #   self, 'Communication', "Communication Moteur")


    def quitter(self):
        """
        =======================================================================#
            Fonction qui permet de quitter l'application                       #
        =======================================================================#
        """
        self.close()


if __name__ == "__main__":
    # Ligne obligatoire pour cliquer avec un macbook
    QApplication.setStyle("Fusion")
    # Ligne obligatoire pour lancer ma fenêtre
    appli = QApplication(sys.argv)
    # appli.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())
    # Variable pour l'acquisition de mes données dans ma bdd
    print("lol")
    # je récupère ma class et je l'affiche
    affichage = Display()
    affichage.show()
    appli.exec_()
